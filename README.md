[![pipeline status](https://gitlab.com/mausy5043/uss_cygnus/badges/freebsd/pipeline.svg)](https://gitlab.com/mausy5043/uss_cygnus/commits/freebsd)

# USS_Cygnus

[For those old enough to remember it ...](https://en.wikipedia.org/wiki/The_Black_Hole)

The `freebsd` branch assumes it is used with OPNsense on FreeBSD 11.1 with `dnsmasq` as the DNS.
Testing, however, is done on a Debian system. So, this may work there too.

This branch no longer requires a host computer to run on. Instead, it is designed to run inside a GitLab Runner and output the lists periodically.

## Installing
Don't install locally.

## White.list
The domains provided with the `white.list.sources.txt` file are based on one or more of the following considerations:
The domains in the `white.list` file are based on one or more of the following considerations:
1. They are required for this app to work because they contain the sources of the lists,
2. They offer (essential) services that should not be blocked,
3. They are recommended [here](https://firebog.net/) and/or [here](https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212).

## Procedure
The app works as follows:
  - Download all sources
  - Compile a single list of sites to be blocked
  - Clean-up the list, removing duplicates, empty lines, comments lines, etc.
  - Remove the sites that are in the app's `white.list.sources.txt`
  - Add the sites listed in the user's blacklist stored locally in `~/.config/cygnus/black.list`. This allows users to override the app's whitelisting any undesirable sites. (THIS MAY BREAK THE APP)
  - Remove any sites listed in the user's whitelist stored locally in `~/.config/cygnus/white.list`. This allows users to override the app's blacklisting any sites that the user requires. (THIS MAY INTRODUCE A SECURITY RISK)
  - Clean-up once more
  - Convert the list to `hosts` format and put it in the right place.

## Tiers of precedence
  - Tier 0 is the list of blacklists `black.list.sources.txt`. Theoretically all the sites/domains listed in those lists are blocked. The lists are cleaned first. Malformed addresses and other unusable lines (comments; white-space) are removed.
  - Tier 1 is the application's list of whitelists `white.list.sources.txt`. These are essential sites/domains that should not be blocked.
  - Tier 2: The user may want to block additional sites for personal reasons. The user can provided a list through a user-managed file `~/.config/cygnus/black.list`
  - Tier 3: The user may also want to unblock sites that may be blocked erroneously. The user can provide a list thorough the file `~/.config/cygnus/white.list`

>>>>>>> updated-acquisition
