#!/usr/bin/env python3
"""Get some statistics from the blocklist."""

# import os
# import socket
import sys
# from fileinput import input
import filter as fltr

ENC = sys.getdefaultencoding()


def main():
    """Execute main loop."""
    newlines = []
    ifile = fltr.get_cli_params()
    lines = fltr.read_file(ifile)
    pointer = 0
    # revlines = [x.split(".")[::-1] for x in lines].sort(key = operator.itemgetter(1, 2))

    for domain in lines:
        pointer += 1
        niamod = '.'.join(domain.split('.')[::-1])
        newlines.append(niamod)
    newlines.sort()

    write_file(ifile, newlines)


def write_file(file_to_write_to, lines_to_write):
    """
    Output <lines_to_write> to the file <file_to_write_to>.

    Will overwrite existing file.
    """
    with open(file_to_write_to, 'w', encoding=ENC) as output_file:
        for line in lines_to_write:
            output_file.write('{0}\n'.format(line))


if __name__ == '__main__':
    main()
