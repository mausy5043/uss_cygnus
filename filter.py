#!/usr/bin/env python3

"""Filter a hosts file, removing useless lines.

Reads a flat textfile and converts it.
Lines that seem to contain Unicode are attempted to be read as LATIN-1.

Removes:
- empty lines
- comments (lines starting with "#"" or ";"")
- lines containing a ";" are trimmed
- HTML (lines starting with "<"")
- lines containing a "<" are trimmed
- leading and trailing whitespace
- leading IP4 or IP6 addresses
- trailing portnumbers

Extracts the domainnames from any http(s); ftp(s); mailto URLs
Tries to convert naked IP addresses to FQDN.

"""

import os
import socket
import sys

ENC = sys.getdefaultencoding()


def get_cli_params():
    """Check for presence of a CLI parameter."""
    if len(sys.argv) != 2:
        sys.exit(0)
    # 1 parameter required = filename to be processed
    return sys.argv[1]


def read_file(file_to_read_from):
    """
    Return the contents of a file if it exists.

    Abort if it doesn't exist.
    """
    if not os.path.isfile(file_to_read_from):
        sys.exit(0)
    try:
        with open(file_to_read_from, 'r', encoding=ENC) as input_file:
            # read the inputfile
            return input_file.read().splitlines()
    except UnicodeDecodeError:
        # print(" AS LATIN-1 ")
        with open(file_to_read_from, 'r', encoding='latin-1') as input_file:
            # read the inputfile
            return input_file.read().splitlines()


def write_file(file_to_write_to, lines_to_write):
    """
    Output <lines_to_write> to the file <file_to_write_to>.

    Will overwrite existing file.
    """
    with open(file_to_write_to, 'w', encoding=ENC) as output_file:
        for line in lines_to_write:
            output_file.write('{0}\n'.format(line.decode('utf-8')))


def flatten(line):
    """
    Reduce the input to something more simple.

    Comment lines and empty lines return as a single hash (#)
    Lines containing IP-adress-look-a-likes return as just the site/domainname
    """
    # check for specific first characters
    if line.startswith("#"):
        line = "#"
    if line.startswith("<"):
        # print("      rejecting (<*)     : {0}".format(line))
        line = "#"
    if line.startswith(";"):
        # print("      rejecting (;*)     : {0}".format(line))
        line = "#"

    # check for specific characters inside the string
    if ";" in line:
        line = line.split(";")[0]
    if "<" in line:
        line = line.split("<")[0]

    # see if we have a valid FQDN
    fields = line.split()
    number_of_fields = len(fields)
    sitename = "#"
    if number_of_fields == 1:
        # NOK: the first cell is not an IP-address, assume it is a host/domain
        sitename = fields[0]
    if number_of_fields > 1:
        sitename = fields[1]
    # some sources annoyingly add ";1" behind the domainname
    sitename = sitename.split(';')[0]
    return sitename


def validate(domain):
    """Remove invalid entries."""
    if domain.startswith(("http://", "https://", "ftp://", "ftps://")):
        domain = dehttp(domain)
    if domain.startswith((".", ":", "!", "|", "/", ";", "=", "<", "mailto:")):
        # print("      rejecting (multi)  : {0}".format(domain))
        return "#"
    if ":" in domain:
        # remove portnumber
        domain = domain.split(":")[0]
    if is_valid_ip4(domain):
        # IP-addresses should not be on the list
        # print("      rejecting (IP)     : {0}".format(domain))
        temp_0 = socket.getfqdn(domain)
        if temp_0 is not domain:
            domain = temp_0.replace('unspecified.', '')
            # print("\nAdding FQDN : {0}".format(domain))
        else:
            # print("\nIgnoring IP : {0}".format(temp_0))
            return "#"
    if domain.endswith("."):
        domain = domain[:-1]
    if len(domain.split(".")) == 1:
        # print("      rejecting (.)      : {0}".format(domain))
        return "#"
    return domain


def dehttp(url):
    """Extract the domainname from a URL."""
    return url.split('/')[2]


def is_valid_ip4(addr):
    """Test if a given string is a valid IP4-address or not."""
    try:
        socket.inet_aton(addr)
        return True
    except socket.error:
        return False


def main():
    """Execute main loop."""
    newlines = []
    ifile = get_cli_params()
    lines = read_file(ifile)
    pointer = 0
    simplified_stripped_lcase = "#"
    for line in lines:
        pointer += 1
        # remove any leading or trailing whitespace and convert to lowercase
        stripped_lcase = line.strip().lower()
        # check if there is something left
        if stripped_lcase:
            # flatten the string to a single "word".
            simplified_stripped_lcase = flatten(stripped_lcase)
        else:
            # empty lines are replaced by "#", not to worry. "#" are removed later
            simplified_stripped_lcase = "#"
        if not simplified_stripped_lcase.strip().startswith("#"):
            domain = validate(simplified_stripped_lcase)
        else:
            domain = "#"
        # output the result; excluding "#"
        if not domain.startswith("#"):
            try:
                site_name = domain.strip().encode('idna')
            except UnicodeDecodeError:
                pass
            except UnicodeError:
                pass
            newlines.append(site_name)
    newlines.sort()

    write_file(ifile, newlines)


if __name__ == '__main__':
    main()
