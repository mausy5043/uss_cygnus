#!/usr/bin/env bash

# V.I.N.CENT

if [[ "$1" = "-t" ]]; then
  echo ">>>>> TEST MODE SELECTED <<<<<"
  TEST=1
else
  TEST=0
fi

# Find out where we're running from
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# shellcheck source=bin/include
# shellcheck disable=SC1091
source "${SCRIPT_DIR}/bin/include"

# USS Cygnus's sources of blocklists are very divers:
# Sources, one per line:
CYGNUS_BLACK_LISTS="${SCRIPT_DIR}/black.list.sources.txt"

# USS Cygnus allows your own blacklist and will add it if it exists.
##CYGNUS_LOCALBLACK_LIST="${CYGNUS_CONFIG_DIR}/black.list"
# USS Cygnus allows your personal whitelist and will process this last of all.
##CYGNUS_LOCALWHITE_LIST="${CYGNUS_CONFIG_DIR}/white.list"
# USS Cygnus also comes with it's own whitelists to ensure the source sites
# don't get blocked and to unblock useful sites.
CYGNUS_WHITE_LISTS="${SCRIPT_DIR}/white.list.sources.txt"

# Cleanup previous data
rm -rf /tmp/cygnus.*

# Where to put the output
##CYGNUS_OUTPUT=$(mktemp /tmp/cygnus.list.XXXXXX)

# A temporary file for storing the intermediate results
DLD_B_DIR=$(mktemp -d /tmp/cygnus.black.XXXXXX)
DLD_W_DIR=$(mktemp -d /tmp/cygnus.white.XXXXXX)
# clean up
if [[ ! -d "${TGT_B_DIR}" ]]; then
  mkdir "${TGT_B_DIR}"
else
  rm "${TGT_B_DIR}/*"
fi

if [[ ! -d "${TGT_W_DIR}" ]]; then
  mkdir "${TGT_W_DIR}"
else
  rm "${TGT_W_DIR}/*"
fi

if [[ ! -d "${FIN_DIR}" ]]; then
  mkdir "${FIN_DIR}"
else
  rm "${FIN_DIR}/*"
fi

function fn_fetch () {
  # parsing the source file $SOURCE_FILE for URLs
  SOURCE_FILE="${1}"
  DLD_DIR="${2}"
  echo ""
  echo "Fetching the lists from ${SOURCE_FILE}"
  while read -r URL; do
    fn_download "${URL}" "${DLD_DIR}"
  done < "${SOURCE_FILE}"
}

function fn_download () {
  # download a file $URL to a given location $DLD_DIR
  URL="${1}"
  DLD_DIR="${2}"
  if [[ "${URL}" != \#* ]]; then
    DLD_FILE="${DLD_DIR}/${URL//[:\/\&\?\=;]/-}"
    echo -ne "  ${INFO} Downloading ${URL}"
    if wget --user-agent='Mozilla/5.0 (wget)' --timeout=20 --quiet --output-file='/tmp/wget.log' -O "${DLD_FILE}" "${URL}" >>/tmp/wget.out ; then
      echo -e "${OVER}  ${TICK} Downloaded  ${DLD_FILE}"
    else
      echo -e "${OVER}  ${CROSS} Download of ${URL} FAILED"
    fi
  fi
}

function fn_copyraw () {
  # make a copy of the downloaded files for further processing
  echo ""
  echo "Creating a copy..."
  cp -r "${DLD_B_DIR}"/* "${TGT_B_DIR}"/ || exit 1
  cp -r "${DLD_W_DIR}"/* "${TGT_W_DIR}"/ || exit 1
}

function fn_filter () {
  TGT_DIR="${1}"
  # pass each file in the given directory $TGT_DIR through the filter
  echo ""
  echo "Filtering the files in ${TGT_DIR}..."
  # !! Files are filtered in-situ !!
  for FLTR_FILE in "${TGT_DIR}"/*; do
    echo -ne "  ${INFO} Filtering ${FLTR_FILE}"
    if ./filter.py "${FLTR_FILE}"; then
        echo -e "${OVER}  ${TICK} Filtered  ${FLTR_FILE}"
      else
        echo -e "${OVER}  ${CROSS} Filtering of ${FLTR_FILE} FAILED" && exit 1
    fi
  done
}

function fn_sortuniq () {
  TGT_DIR="${1}"
  # sort the entries in each file and remove any duplicates (only duplicates inside the file are removed, not between files)
  local TMP_FILE
  TMP_FILE=$(mktemp /tmp/cygnus.XXXXXX)
  echo ""
  echo "Sorting the files in ${TGT_DIR}..."
  for SRT_FILE in "${TGT_DIR}"/*; do
    echo -ne "  ${INFO} Sorting ${SRT_FILE}"
    mv "${SRT_FILE}" "${TMP_FILE}"
    sort "${TMP_FILE}" | uniq > "${SRT_FILE}"
    echo -e "${OVER}  ${TICK} Sorted  ${SRT_FILE}"
  done
  rm "${TMP_FILE}"
}

function fn_rd_stitch () {
  TGT_DIR="${1}"
  TGT_FILE="${2}"
  # stitch all the files together
  # sort them in reverse-domain order and
  # remove all duplicates
  echo ""
  echo "Concatenating results into ${TGT_FILE}..."
  cat "${TGT_DIR}"/* 2>/dev/null \
   | awk -F . '{for(i=NF;i>0;i--)printf("%s ",$i);printf(" %s\n",$0)}' \
   | sort \
   | awk '{print $NF}' \
   | uniq > "${TGT_FILE}"
}

function fn_stitch () {
  TGT_DIR="${1}"
  TGT_FILE="${2}"
  # stitch all the files together
  # sort them and remove all duplicates
  echo ""
  echo "Concatenating results into ${TGT_FILE}..."
  cat "${TGT_DIR}"/* 2>/dev/null \
   | sort \
   | uniq > "${TGT_FILE}"
}

function fn_split () {
  # split domains and hosts
  # as a rule-of-thumb domains are defined as lines that only have one node under the top-level
  # eg.: foo.bar
  # while hosts are the lines that have more than one node
  # eg.: foobar.foo.bar  or  fb.foobar.foo.bar
  echo ""
}

function fn_user_list () {
  SRC_FILE="${1}"
  TGT_FILE="${2}"
  touch "${TGT_FILE}"
  local TMP_FILE
  TMP_FILE=$(mktemp /tmp/cygnus.XXXXXX)
  if [[ -f "${SRC_FILE}" ]]; then
    cp  "${SRC_FILE}" "${TGT_FILE}"
    ./filter.py  "${TGT_FILE}"
    mv "${TGT_FILE}" "${TMP_FILE}"
    sort "${TMP_FILE}" | uniq > "${TGT_FILE}"
  fi
  rm "${TMP_FILE}"
}

# Workflow:
# Fetch all white lists
fn_fetch "${CYGNUS_WHITE_LISTS}" "${DLD_W_DIR}"
# Fetch all black lists
fn_fetch "${CYGNUS_BLACK_LISTS}" "${DLD_B_DIR}"
# Make a copy of the blacklists and the whitelists for further processing
fn_copyraw
# Filter the whitelists
fn_filter "${TGT_W_DIR}"
# Filter the blacklists
fn_filter "${TGT_B_DIR}"
# Sort the whitelists
fn_sortuniq "${TGT_W_DIR}"
# Sort the blacklists
fn_sortuniq "${TGT_B_DIR}"
# Add all whitelists together (sort the resulting list and remove duplicates)
fn_stitch "${TGT_W_DIR}" "${TGT_W_DIR}/whitied.out"
# Add all blacklists together (sort the resulting list and remove duplicates)
fn_stitch "${TGT_B_DIR}" "${TGT_B_DIR}/blacked.out"

# Remove the whitelisted sites from the blacklist
comm -13 "${TGT_W_DIR}/whitied.out" "${TGT_B_DIR}/blacked.out" > "${FIN_DIR}/blacked.out"

# Add the local blacklist
# TODO: use the bin/dnsblack script to parse the local list

# this creates a filtered and sorted copy of the user's local blacklist
# which is then added to the existing master blacklist (DUPLICATES ARE REMOVED)
fn_user_list "${CYGNUS_CONFIG_DIR}/black.list" "${FIN_DIR}/local.black.list"
fn_stitch "${FIN_DIR}" "${FIN_DIR}/black.out"

# subtract the local whitelist
# TODO: output the blacklists that carry a whitelisted site
# TODO: use the bin/dnswhite script to parse the local list

# this creates a filtered and sorted copy of the user's local whitelist
# which is then removed from the existing master blacklist
fn_user_list "${CYGNUS_CONFIG_DIR}/white.list" "${FIN_DIR}/local.white.list"
comm -13 "${FIN_DIR}/local.white.list" "${FIN_DIR}/black.out" >"${FIN_DIR}/sungyc.out"

df -h /tmp
wc -l "${FIN_DIR}/sungyc.out"

if [ $TEST = 0 ]; then
  sleep 1
  # delete the raw blacklists
  rm -rf "${DLD_B_DIR}"
  # delete the raw whitelists
  rm -rf "${DLD_W_DIR}"
fi

cp "${FIN_DIR}/sungyc.out" ./sungyc.out
